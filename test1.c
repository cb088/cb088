#include<stdio.h>
#include<math.h>
int main() /*Created by Parjanya Modi*/
{
	float a, b, c, d, w;
	printf("Program to find out the distance between two points \n");
	printf("Enter x coordinate of first point\n");
	scanf("%f", &a);
	printf("Enter y coordinate of first point\n");
	scanf("%f", &b);
	printf("Enter x coordinate of second point\n");
	scanf("%f", &c);
	printf("Enter y coordinate of second point\n");
	scanf("%f", &d);
	w=sqrt(((a-c)*(a-c))+((b-d)*(b-d)));
	printf("The distance between given two points is %f", w);
	return 0;
}
