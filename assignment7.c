#include <stdio.h> 
int main () 
{   int ne, n[10][10], sd = 0, sbd = 0, sad = 0, i, j;   
	printf("This program prints the sum of elements of diagonal, above it and below it of a square matrix.\n");   
	printf ("Enter the dimension of area.(Note: This is a Square matrix.\n");   
	scanf ("%d", &ne);   
	printf ("Enter the elements of the matrix.\n");   
	for (i = 0; i < ne; i++)
    	{       
			for (j = 0; j < ne; j++)
  			{
    			scanf ("%d", &n[i][j]);
     		}     
		}   
		for (i = 0; i < ne; i++)
    	{       
			for (j = 0; j < ne; j++)
  			{
    			if (i == j)
      			{
        		sd = sd + n[i][j];         
				if (i + 1 == ne || j + 1 == ne)
    				continue;
        		else
    			{
      				sbd = sbd + n[i + 1][j];       
					sad = sad + n[i][j + 1];
    			}
      		}
  		}     
	}
  printf ("Sum of elements of diagonal: %d", sd);   
  printf ("\nSum of elements below diagonal: %d", sbd);   
  printf ("\nSum of elements above diagonal: %d", sad);
}
