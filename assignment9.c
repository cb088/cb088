#include <stdio.h>
#include <stdlib.h>
 
typedef struct{
 	char name[30];
    int id;     
	long salary;  
} Employee;  

int main() 
{
    int i, n;
 
    Employee employees[10];     
	printf("Enter the no. of employees.\n");     
	scanf("%d",&n);     
	printf("Enter %d Employee Details \n \n",n);     
	for(i=0; i<n; i++)
	{
		printf("Employee %d:- \n",i+1);
        printf("Name: ");
       	scanf("%s",&employees[i].name);
        printf("Id: ");
        scanf("%d",&employees[i].id);
        printf("Salary: ");
        scanf("%ld",&employees[i].salary);
        printf("\n");
    }
	printf("Details of Employees whose salary is less than INR 25000. \n");
 
	for(i=0; i<n; i++)
	{
       if(employees[i].salary <= 25000)
	   {
        printf("Name \t: ");
        printf("%s \n",employees[i].name);
        printf("Id \t: ");
        printf("%d \n",employees[i].id);
        printf("Salary \t: ");         
		printf("%ld \n",employees[i].salary);
        printf("\n");
       }
    }  
    return 0;
 
}
