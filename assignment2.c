//Created by Parjanya Modi
//Program for area of rectangle using function 
#include <stdio.h>

float areaRectangle(float, float);

int main()
{
    float l, b, area;
    printf("Enter the length : ");
    scanf("%f", &l);

    printf("Enter the width : ");
    scanf("%f", &b);

    area = areaRectangle(l, b);

    printf("The area of the rectangle : %f", area);

    return 0;
}

float areaRectangle(float length, float width)
{
    return length * width;
}